package art.ozandmlr.game.universal.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Disposable;

/**
 * Wallpaper makes it easy to work with the background of the window,
 * draw the texture in different modes
 */
public class Wallpaper implements Disposable {

    private static final String TAG = Wallpaper.class.getName();

    public enum DrawMode {
        /**
         * The texture will be centered on the screen
         */
        CENTER,

        /**
         * The texture will be stretched to the full window
         */
        SCREEN,

        /**
         * The texture will be repeated
         * <b>starting at the bottom left corner</b>
         */
        REPEAT
    }

    private final Texture texture;
    private DrawMode drawMode;
    private float width;
    private float height;

    /**
     * Create new Wallpaper,
     * where the parameter mode is CENTER and
     * the parameters width and height initially have values like texture
     *
     * @param texture Textured to be used as wallpaper
     */
    @SuppressWarnings("WeakerAccess")
    public Wallpaper(Texture texture) {
        this(texture, DrawMode.CENTER, texture.getWidth(), texture.getHeight());
    }

    /**
     * Create new Wallpaper,
     * where the parameter mode is CENTER
     *
     * @param texture Textured to be used as wallpaper
     * @param width   Texture width does not matter in REPEAT mode
     * @param height  Texture height does not matter in REPEAT mode
     */
    @SuppressWarnings("WeakerAccess")
    public Wallpaper(Texture texture, float width, float height) {
        this(texture, DrawMode.CENTER, width, height);
    }

    /**
     * Create new Wallpaper,
     * where the parameters width and height initially have values like texture
     *
     * @param texture Textured to be used as wallpaper
     * @param mode    Wallpaper drawing mode
     */
    @SuppressWarnings("WeakerAccess")
    public Wallpaper(Texture texture, DrawMode mode) {
        this(texture, mode, texture.getWidth(), texture.getHeight());
    }

    /**
     * Create new Wallpaper
     *
     * @param texture Textured to be used as wallpaper
     * @param mode    Wallpaper drawing mode
     * @param width   Texture width does not matter in REPEAT mode
     * @param height  Texture height does not matter in REPEAT mode
     */
    @SuppressWarnings("WeakerAccess")
    public Wallpaper(Texture texture, DrawMode mode, float width, float height) {
        this.texture = texture;
        this.drawMode = mode;
        this.width = width;
        this.height = height;

        if (this.width == 0) this.width++;
        if (this.height == 0) this.height++;

        Gdx.app.debug(TAG,"Wallpaper created with texture = [" + texture + "], mode = [" + mode + "], width = [" + this.width + "], height = [" + this.height + "]");
    }

    /**
     * Draws this texture like wallpaper
     *
     * @param batch wallpaper texture renderer
     */
    public void draw(Batch batch) {
        switch (drawMode) {
            case CENTER:
                batch.draw(texture, Gdx.graphics.getWidth() / 2f - width / 2f, Gdx.graphics.getHeight() / 2f - height / 2f,width,height);
                break;
            case SCREEN:
                batch.draw(texture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
                break;
            case REPEAT:
                for (int x = 0; x <= Gdx.graphics.getWidth(); x += width)
                    for (int y = 0; y <= Gdx.graphics.getHeight(); y += height)
                        batch.draw(texture, x, y, width, height);
                break;
        }
    }

    public Texture getTexture() {
        return texture;
    }

    public DrawMode getDrawMode() {
        return drawMode;
    }

    public void setDrawMode(DrawMode drawMode) {
        this.drawMode = drawMode;
        Gdx.app.debug(TAG,"setDrawMode, set drawMode = [" + drawMode + "]");
    }

    public void setSize(float width, float height) {
        this.width = width;
        this.height = height;

        if (this.width == 0) this.width++;
        if (this.height == 0) this.height++;
        Gdx.app.debug(TAG,"setSize, set width = [" + this.width + "], height = [" + this.height + "]");
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
        Gdx.app.debug(TAG,"setWidth, set width = [" + this.width + "]");
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
        Gdx.app.debug(TAG,"setHeight, set height = [" + this.height + "]");
    }

    @Override
    public void dispose() {
        texture.dispose();
        Gdx.app.debug(TAG,"dispose");
    }
}
