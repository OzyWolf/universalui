package art.ozandmlr.game.universal.ui.loader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;
import com.badlogic.gdx.utils.Queue;

/**
 * resource file loader for program operation
 */
class Downloader extends Thread {

    private static final String TAG = Downloader.class.getName();

    private final String url;
    private final Queue<String> queue;
    private int max;
    private int current;

    /**
     * @param url link to the project directory with textures for download
     */
    Downloader(String url) {
        this.url = url;
        queue = new Queue<String>();
    }

    /**
     * adds files to download queue
     *
     * @param name download file name
     */
    public void addToQueue(String name) {
        queue.addLast(name);
        Gdx.app.debug(TAG, "addToQueue" + "name = [" + name + "]");
    }

    /**
     * Creates a request to download and write all files in the queue
     */
    private void download() {
        max = queue.size;
        current = 0;
        while (!queue.isEmpty()) {
            final String file = queue.removeFirst();
            Gdx.app.debug(TAG, "Starting download " + url + file);
            Net.HttpRequest request = new HttpRequestBuilder().newRequest().method("GET").url(url + file).build();
            Net.HttpResponseListener listener = new DownloadResponse(file, this);
            Gdx.net.sendHttpRequest(request, listener);
            synchronized (this) {
                try {
                    Gdx.app.debug(TAG, "waiting download finish");
                    this.wait();
                    Gdx.app.debug(TAG, "download finish");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            current++;
        }
    }

    /**
     * @return current progress of downloading files
     */
    float getProgress() {
        synchronized (this) {
            if (max != 0) {
                return ((float) 1 / max) * (current);
            } else
                return 0;
        }
    }

    @Override
    public void run() {
        download();
    }
}
