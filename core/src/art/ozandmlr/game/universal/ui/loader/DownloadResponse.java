package art.ozandmlr.game.universal.ui.loader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;

/**
 * Write response as file
 */
class DownloadResponse implements Net.HttpResponseListener {

    private static final String TAG = DownloadResponse.class.getName();

    private final String fileName;
    private final Downloader downloader;

    /**
     * @param fileName Name of file to create
     * @param downloader Downloader Threat to stop
     */
    DownloadResponse(String fileName, Downloader downloader){
        this.fileName = fileName;
        this.downloader = downloader;
    }

    @Override
    public void handleHttpResponse(Net.HttpResponse httpResponse) {
        Gdx.files.local(fileName).write(httpResponse.getResultAsStream(), false);
        Gdx.app.log(TAG, "handleHttpResponse statusCode = [" + httpResponse.getStatus().getStatusCode() + "]");
        synchronized (downloader){
            downloader.notify();
        }
    }

    @Override
    public void failed(Throwable t) {
        Gdx.app.log(TAG, "failed " + t.getMessage());
        t.printStackTrace();
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (Gdx.files.local(fileName).exists())
                    Gdx.files.local(fileName).delete();
                Gdx.app.exit();
            }
        });
    }

    @Override
    public void cancelled() {
        Gdx.app.log(TAG, "cancelled");
        Gdx.app.postRunnable(new Runnable() {
            @Override
            public void run() {
                if (Gdx.files.local(fileName).exists())
                    Gdx.files.local(fileName).delete();
                Gdx.app.exit();

            }
        });
    }
}
