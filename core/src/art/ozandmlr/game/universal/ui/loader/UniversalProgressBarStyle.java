package art.ozandmlr.game.universal.ui.loader;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Disposable;

/**
 * universal style loader. consisting of 4 textures lying on the internal storage
 */
class UniversalProgressBarStyle extends ProgressBar.ProgressBarStyle implements Disposable {

    private final TextureRegion pbg;
    private final TextureRegion pk;
    private final TextureRegion pa;
    private final TextureRegion pb;

    UniversalProgressBarStyle(){
        pbg = new TextureRegion(new Texture("assets/progressbar_b.png"));
        pk = new TextureRegion(new Texture("assets/progressbar_pk.png"));
        pa = new TextureRegion(new Texture("assets/progressbar_ak.png"));
        pb = new TextureRegion(new Texture("assets/progressbar_bk.png"));
        this.background = new TextureRegionDrawable(pbg);
        this.knob = new TextureRegionDrawable(pk);
        this.knobAfter = new TextureRegionDrawable(pa);
        this.knobBefore = new TextureRegionDrawable(pb);
    }

    @Override
    public void dispose() {
        pbg.getTexture().dispose();
        pk.getTexture().dispose();
        pa.getTexture().dispose();
        pb.getTexture().dispose();
    }
}
