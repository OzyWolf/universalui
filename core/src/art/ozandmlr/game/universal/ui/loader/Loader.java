package art.ozandmlr.game.universal.ui.loader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.Arrays;

public abstract class Loader implements Screen {

    private static final String UI_NAME = "ui";
    private static final String TAG = Loader.class.getName();

    private final AssetManager assetManager;
    private final Stage stage;
    private final ProgressBar progressBar;
    private final Downloader downloader;


    /**
     * @param texturesURL link to the project directory with textures for download
     * @param gameTextures additional file names
     */
    public Loader(String texturesURL, String... gameTextures) {
        Gdx.app.debug(TAG, "texturesURL = [" + texturesURL + "], gameTextures = [" + Arrays.toString(gameTextures) + "]");
        assetManager = new AssetManager();
        stage = new Stage(new ScreenViewport());
        downloader = new Downloader(texturesURL);
        progressBar = new ProgressBar(0,1,0.01f,false,new UniversalProgressBarStyle());
        load(gameTextures);
        if (checkFiles(gameTextures))
            download(gameTextures);
    }

    private boolean checkFiles(String... url) {
        if (!(Gdx.files.local(UI_NAME + ".png").exists() && Gdx.files.local(UI_NAME + ".atlas").exists()))
            return true;

        for (String anUrl : url)
            if (!(Gdx.files.local(anUrl + ".png").exists() && Gdx.files.local(anUrl + ".atlas").exists()))
                return true;
        return false;
    }

    private void load(String... url) {
        assetManager.load(UI_NAME + ".atlas", TextureAtlas.class);
        for (String anUrl : url) {
            assetManager.load(anUrl + ".atlas", TextureAtlas.class);
        }
    }

    private void download(String... url) {
        downloader.addToQueue(UI_NAME + ".png");
        downloader.addToQueue(UI_NAME + ".atlas");
        for (String anUrl : url) {
            downloader.addToQueue(anUrl + ".png");
            downloader.addToQueue(anUrl + ".atlas");
        }
        downloader.start();
    }

    /**
     * action at the end of the download
     */
    @SuppressWarnings("WeakerAccess")
    public abstract void onLoadFinish();

    public AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void show() {
        stage.clear();
        progressBar.setWidth(Gdx.graphics.getWidth());
        stage.addActor(progressBar);
        progressBar.setPosition(0, 10);
        Gdx.app.log(TAG,Gdx.files.getLocalStoragePath());
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (!downloader.isAlive()) {
            if (assetManager.update()) {
                onLoadFinish();
            }
            progressBar.setValue(assetManager.getProgress());
        }else {
            progressBar.setValue(downloader.getProgress());
        }
        Gdx.app.debug(TAG, "Loading assets progress - " + progressBar.getPercent());
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
