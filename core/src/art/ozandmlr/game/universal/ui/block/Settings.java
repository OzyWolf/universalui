package art.ozandmlr.game.universal.ui.block;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;

import java.util.ArrayList;

/**
 * Block with checkbox buttons
 */
public class Settings extends Block {

    private static final String TAG = Settings.class.getName();

    private final CheckBox.CheckBoxStyle checkBoxStyle;

    /**
     * Creates a settings(CheckBox) menu.
     * pad is 0
     * height is 0
     * x is 0
     * y is 0
     *
     * @param width block width
     */
    @SuppressWarnings("WeakerAccess")
    public Settings(int width, CheckBox.CheckBoxStyle checkBoxStyle) {
        this(0, 0, width, 0,checkBoxStyle);
    }

    /**
     * Creates a settings(CheckBox) menu.
     * The height value is initially 2*pad
     * x is 0
     * y is 0
     *
     * @param width block width
     * @param pad   from edge
     */
    @SuppressWarnings("WeakerAccess")
    public Settings(int width, int pad, CheckBox.CheckBoxStyle checkBoxStyle) {
        this(0, 0, width, pad,checkBoxStyle);
    }

    /**
     * Creates a settings(CheckBox) menu.
     * pad is 0
     * height is 0
     *
     * @param x     x position
     * @param y     y position
     * @param width block width
     */
    @SuppressWarnings("WeakerAccess")
    public Settings(int x, int y, int width, CheckBox.CheckBoxStyle checkBoxStyle) {
        this(x, y, width, 0, checkBoxStyle);
    }

    /**
     * Creates a settings(CheckBox) menu.
     * The height value is initially 2*pad
     *
     * @param x     x position
     * @param y     y position
     * @param width block width
     * @param pad   from edge
     */
    public Settings(int x, int y, int width, int pad, CheckBox.CheckBoxStyle checkBoxStyle) {
        this.checkBoxStyle = checkBoxStyle;
        this.x = x;
        this.y = y;
        this.width = width;
        this.pad = (int) (pad * Gdx.graphics.getDensity());

        elementList = new ArrayList<Actor>();
        height = 2 * pad;
    }

    @Override
    public void addElement(String text, EventListener listener) {
        CheckBox checkBox = new CheckBox(text,checkBoxStyle);
        if (listener == null)
            checkBox.setText("Coming Soon (" + text + ")");
        else
            checkBox.addListener(listener);
        checkBox.setSize(width - 2 * pad, getButtonHeight() * Gdx.graphics.getDensity());
        checkBox.pad(5);
        elementList.add(checkBox);
        height += (getButtonHeight() + pad) * Gdx.graphics.getDensity();

        Gdx.app.debug(TAG, "addButton, add another button with text = [" + text + "], listener = [" + listener + "]");
    }
}
