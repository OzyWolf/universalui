package art.ozandmlr.game.universal.ui.block;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;

import java.util.List;

public abstract class Block {

    private static final String TAG = Block.class.getName();
    private static final int BUTTON_HEIGHT = 128;

    int x, y, width, height, pad;
    List<Actor> elementList;

    /**
     * Adds another button to the menu on top
     *
     * @param text     Button text
     * @param listener In the case of pressing the mouse button performs the specified action. If NULL, the button acquires the text "coming soon"
     */
    abstract public void addElement(String text, EventListener listener);

    /**
     * linking the menu to the stage
     *
     * @param stage for binding
     */
    public void connectWithStage(Stage stage) {
        for (int i = 0; i < elementList.size(); i++) {
            stage.addActor(elementList.get(i));
            elementList.get(i).setPosition(x + pad, y + (i) * (pad + BUTTON_HEIGHT * Gdx.graphics.getDensity()));
            Gdx.app.debug(TAG, "connectWithStage, add button to stage");
        }
        Gdx.app.log(TAG, "connectWithStage, connected with stage = [" + stage + "]");
    }

    public void setPosition(int x, int y) {
        this.x = x;
        this.y = y;

        for (int i = 0; i < elementList.size(); i++) {
            elementList.get(i).setPosition(x + pad, y + (i) * (pad + BUTTON_HEIGHT * Gdx.graphics.getDensity()));
        }

        Gdx.app.debug(TAG, "setPosition, Menu position now is x = [" + x + "], y = [" + y + "]");
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;

        for (int i = 0; i < elementList.size(); i++) {
            elementList.get(i).setPosition(x + pad, y + (i) * (pad + BUTTON_HEIGHT * Gdx.graphics.getDensity()));
        }

        Gdx.app.debug(TAG, "setPosition, x = [" + x + "] now");
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;

        for (int i = 0; i < elementList.size(); i++) {
            elementList.get(i).setPosition(x + pad, y + (i) * (pad + BUTTON_HEIGHT * Gdx.graphics.getDensity()));
        }

        Gdx.app.debug(TAG, "setPosition, y = [" + y + "] now");
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;

        for (Actor anElementList : elementList) {
            anElementList.setWidth(width);
        }

        Gdx.app.debug(TAG, "setPosition, width = [" + width + "] now");
    }

    public int getHeight() {
        return height;
    }

    public int getPad() {
        return pad;
    }

    public void setPad(int pad) {
        this.pad = pad;

        for (int i = 0; i < elementList.size(); i++) {
            elementList.get(i).setPosition(x + pad, y + (i) * (pad + BUTTON_HEIGHT * Gdx.graphics.getDensity()));
        }

        Gdx.app.debug(TAG, "setPosition, pad = [" + pad + "] now");
    }

    @SuppressWarnings("WeakerAccess")
    public static int getButtonHeight() {
        return (int) (BUTTON_HEIGHT * Gdx.graphics.getDensity());
    }
}
