package art.ozandmlr.game.universal.ui.block;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

import java.util.ArrayList;

/**
 * Block with buttons
 */
public class Menu extends Block {

    private static final String TAG = Menu.class.getName();

    private final TextButton.TextButtonStyle buttonStyle;

    /**
     * Creates a menu.
     * pad is 0
     * height is 0
     * x is 0
     * y is 0
     *
     * @param width block width
     */
    @SuppressWarnings("WeakerAccess")
    public Menu(int width, TextButton.TextButtonStyle buttonStyle) {
        this(0, 0, width, 0, buttonStyle);
    }

    /**
     * Creates a menu.
     * The height value is initially 2*pad
     * x is 0
     * y is 0
     *
     * @param width block width
     * @param pad   from edge
     */
    @SuppressWarnings("WeakerAccess")
    public Menu(int width, int pad, TextButton.TextButtonStyle buttonStyle) {
        this(0, 0, width, pad, buttonStyle);
    }

    /**
     * Creates a menu.
     * pad is 0
     * height is 0
     *
     * @param x     x position
     * @param y     y position
     * @param width block width
     */
    @SuppressWarnings("WeakerAccess")
    public Menu(int x, int y, int width, TextButton.TextButtonStyle buttonStyle) {
        this(x, y, width, 0, buttonStyle);
    }

    /**
     * Creates a menu.
     * The height value is initially 2*pad
     *
     * @param x     x position
     * @param y     y position
     * @param width block width
     * @param pad   from edge
     */
    @SuppressWarnings("WeakerAccess")
    public Menu(int x, int y, int width, int pad, TextButton.TextButtonStyle buttonStyle) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.pad = (int) (pad * Gdx.graphics.getDensity());
        this.buttonStyle = buttonStyle;

        elementList = new ArrayList<Actor>();
        height = 2 * pad;
    }

    @Override
    public void addElement(String text, EventListener listener) {
        TextButton button = new TextButton(text, buttonStyle);
        if (listener == null)
            button.setText("Coming Soon (" + text + ")");
        else
            button.addListener(listener);
        button.setSize(width - 2 * pad, getButtonHeight() * Gdx.graphics.getDensity());
        elementList.add(button);
        height += (getButtonHeight() + pad) * Gdx.graphics.getDensity();

        Gdx.app.debug(TAG, "addButton, add another button with text = [" + text + "], listener = [" + listener + "]");
    }
}
